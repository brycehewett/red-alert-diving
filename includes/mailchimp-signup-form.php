<div class="container">
  <div class="subscribe-col-1">
    <h2>Subscribe to our Newsletter</h2>
    <h6>Stay informed about news and special promotions.</h6>
  </div>
  <div class="subscribe-col-2">
    <!-- Begin MailChimp Signup Form -->
    <!-- <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css"> -->
    <div id="mc_embed_signup">
      <form
        action="https://redalertdiving.us17.list-manage.com/subscribe/post?u=9c475f433d3fe3a003e5dcb09&amp;id=c0047f65f1"
        method="post" id="mc-embedded-subscribe-form"
        name="mc-embedded-subscribe-form"
        class="validate"
        target="_blank" novalidate>

        <div id="mc_embed_signup_scroll">
          <div class="mc-field-group">
            <input type="email" value="" name="EMAIL" class="required email" placeholder="Email Address" id="mce-EMAIL">
          </div>
          <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
          <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
          </div>
          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
          <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9c475f433d3fe3a003e5dcb09_c0047f65f1" tabindex="-1" value=""></div>
        </div>
      </form>
    </div>
    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[2]='NAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
    <!--End mc_embed_signup-->
  </div>
</div>
