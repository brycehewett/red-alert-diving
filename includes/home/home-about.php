<div class="home__only-the-best">
  <div class="container">
    <h2 class="text__center">ONLY THE BEST.</h2>
    <h3 class="text__center">in Training. in Travel. in Adventure. in Equipment.</h3>
  </div>
</div>
<div class="home__about">
  <div class="container">
    <p>Red Alert Diving is the Florida Panhandle’s newest full-service Dive Center and Aquatic Training Facility. Located in beautiful Panama City Beach, RAD Divers have direct access to miles of uninterrupted white sand beaches, shipwrecks, natural reefs and freshwater springs. Whether you want to see beautiful aquatic life, swim with sharks, or visit relics hidden deep below, our staff can prepare you for it. Red Alert Diving was created with the philosophy of “Only The Best in Training, Travel, Adventure and Equipment.”</p>

    <p>Our instructors are committed to diving excellence and maintain the highest standards in the industry. You have many choices when it comes to your training, and your first consideration should be the same as ours, your safety. Our staff are some of the best in the world with a combined total of more than 50 years spent teaching folks just like you how to safely experience the underwater world. Our philosophy of Only The Best in Training means working with you towards your goals, and ensuring a top end experience, from our heated pool to the open water, leaving you a confident and comfortable diver.</p>
  </div>
</div>
