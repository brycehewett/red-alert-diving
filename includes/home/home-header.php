<div class="home__header-content">
  <h1>The best in Scuba training,<br/>
    Equipment and Adventure.</h1>

  <a class="btn__large btn__tertiary" href="/learn-to-dive">Learn To Dive</a>
  <!--<a class="btn__large btn__tertiary" href="/shop">Shop Dive Equipment</a>-->
</div>
