<div class="home__courses">
  <h2 class="text__center">Our Courses</h2>
  <div class="container">
    <a class="home__courses__course home__courses__course__padi" href="/padi">PADI</a>
    <a class="home__courses__course home__courses__course__dan" href="/dan">DAN</a>
    <a class="home__courses__course home__courses__course__efr" href="/efr">EFR</a>
  </div>
  <div class="container home__courses__action-button-row">
    <a href="https://pcbidc.com/" target="_blank" class="btn__tertiary btn__large">Learn About Training for Professionals <i class="icon ion-share"></i></a>
  </div>
</div>
