<?php

	// Add RSS links to <head> section
	add_theme_support( 'automatic-feed-links' );

  function load_dependencies() {
      //Styles
			wp_enqueue_style( 'material-design-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
			wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );

			//Scripts in the header
      wp_register_script('jQuery', ('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'),'1.11.1', false);

			//Scripts in the footer
			$rad_google_maps_api_key = get_option( 'rad_google_maps_api_key' );

			if ($rad_google_maps_api_key) {
				wp_register_script('map-api', ('https://maps.googleapis.com/maps/api/js?key=' . $rad_google_maps_api_key),false, false);
			}

      wp_register_script( 'custom-functions', get_template_directory_uri() . '/js/functions.min.js', '1.0', true );

			wp_enqueue_script('jQuery');
			wp_enqueue_script('map-api');
			wp_enqueue_script('custom-functions');
  }

  add_action( 'wp_enqueue_scripts', 'load_dependencies' );

	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

	// Remove each style one by one
	add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
	function jk_dequeue_styles( $enqueue_styles ) {
		unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
		return $enqueue_styles;
	}

	// Or just remove them all in one line
	add_filter( 'woocommerce_enqueue_styles', '__return_false' );

	/**
	* Breadcrumbs
	*/

	include_once( 'includes/breadcrumb.php' );

	// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php).
	// Used in conjunction with https://gist.github.com/DanielSantoro/1d0dc206e242239624eb71b2636ab148
	add_filter('add_to_cart_fragments', 'woocommerce_add_to_cart_fragments');

	function woocommerce_add_to_cart_fragments( $fragments ) {
		ob_start();
		include('includes/nav-cart.php');

		$fragments['span.nav-shopping-cart'] = ob_get_clean();

		return $fragments;
	}

	if ( ! function_exists( 'is_woocommerce_activated' ) ) {
		function is_woocommerce_activated() {
			if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
		}
	}

	//Custom Post Type for Team Members
	add_action( 'init', 'create_posttype' );
	function create_posttype() {
		register_post_type( 'team-members',
			array(
				'labels' => array(
					'name' => __( 'Our Team' ),
					'singular_name' => __( 'Team Member' ),
					'add_new_item' => __( 'Add New Team Member' ),
					'edit_item' => __( 'Edit Team Member' ),
					'not_found' => __( 'No team members found' ),
					'not_found_in_trash' => __( 'No team members found in trash' ),
					'view_item' => __( 'View Page' )

				),
				'public' => true,
				'has_archive' => false,
				'publicly_queryable' => false,
				'show_ui' => true,
				'menu_position' => 20,
				'menu_icon' => 'dashicons-groups',
				'supports' => array(
					'title',
					'editor',
					'thumbnail',
					'page-attributes',
					'custom-fields'
					)
			)
		);
		register_post_type( 'FAQ',
			array(
				'labels' => array(
					'name' => __( 'FAQ' ),
					'singular_name' => __( 'Question' ),
					'add_new_item' => __( 'Add New Question' ),
					'edit_item' => __( 'Edit Question' ),
					'not_found' => __( 'No questions found' ),
					'not_found_in_trash' => __( 'No questions found in trash' ),
					'view_item' => __( 'View Question' )
				),
				'public' => false,
				'publicly_queryable' => false,
				'show_ui' => true,
				'has_archive' => false,
				'rewrite' => array('slug' => 'FAQ'),
				'menu_position' => 21,
				'menu_icon' => 'dashicons-editor-help',
				'supports' => array(
					'title',
					'editor',
					'page-attributes',
					)
			)
		);
	}

	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');

    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => __('Sidebar Widgets','RAD' ),
    		'id'   => 'sidebar-widgets',
    		'description'   => __( 'These are widgets for the sidebar.','RAD' ),
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h3>',
    		'after_title'   => '</h3>'
    	));
    }

    // custom menu support
    add_theme_support( 'menus' );
    if ( function_exists( 'register_nav_menus' ) ) {
        register_nav_menus(
            array(
              'primary-menu' => __('Primary Menu', 'RAD'),
							'secondary-menu' => __('Secondary Menu', 'RAD'),
              'footer-menu' => __('Footer Menu', 'RAD'),
							'mobile-nav' => __('Mobile Nav', 'RAD')
            )
        );
    }

    add_theme_support( 'post-thumbnails' );

    //add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.

    function wpe_excerptlength_index($length) {
        return 75;
    }
    function wpe_excerptmore($more) {
        return ' ...';
    }

    function wpe_excerpt($length_callback='', $more_callback='') {
        global $post;

    if(function_exists($length_callback)){
        add_filter('excerpt_length', $length_callback);
    }

    if(function_exists($more_callback)){
        add_filter('excerpt_more', $more_callback);
    }

        $output = get_the_excerpt();
        $output = apply_filters('wptexturize', $output);
        $output = apply_filters('convert_chars', $output);
        $output = '<p>'.$output.'</p>';
        echo $output;
    }

    function short_title() {
        $thetitle = get_the_title();
        $getlength = strlen($thetitle);
        $thelength = 45;

        echo substr($thetitle, 0, $thelength);

        if ($getlength > $thelength) echo "...";
    }

		function rad_customize_register( $wp_customize ) {
			$wp_customize->remove_section( 'custom_css' );
			$wp_customize->remove_section('static_front_page');
		}
		add_action( 'customize_register', 'rad_customize_register' );

		add_action('after_setup_theme', 'remove_admin_bar');

		function remove_admin_bar() {
			if (!current_user_can('administrator') && !is_admin()) {
			  show_admin_bar(false);
			}
		}

		function rad_settings_api_init() {
		 add_settings_field(
			 'rad_google_analytics_ua',
			 'Google Analytics',
			 'rad_google_analytics_ua_callback',
			 'general'
		 );
		 add_settings_field(
			 'rad_google_maps_api_key',
			 'Google Maps API Key',
			 'rad_google_maps_api_key_callback',
			 'general'
		 );
		 add_settings_field(
			 'rad_twitter_url',
			 'Twitter Url',
			 'rad_twitter_url_callback',
			 'general'
		 );
		 add_settings_field(
			 'rad_facebook_app_id',
			 'Facebook App ID',
			 'rad_facebook_app_id_callback',
			 'general'
		 );
		 add_settings_field(
			 'rad_facebook_url',
			 'Facebook Url',
			 'rad_facebook_url_callback',
			 'general'
		 );
		 add_settings_field(
			 'rad_instagram_url',
			 'Instagram Url',
			 'rad_instagram_url_callback',
			 'general'
		 );
		 add_settings_field(
			 'rad_contact_email',
			 'Contact Email',
			 'rad_contact_email_callback',
			 'general'
		 );
		 add_settings_field(
			 'rad_contact_phone',
			 'Contact Phone',
			 'rad_contact_phone_callback',
			 'general'
		 );
		 add_settings_field(
			 'rad_address',
			 'Address',
			 'rad_address_callback',
			 'general'
		 );
		 register_setting( 'general', 'rad_google_analytics_ua' );
		 register_setting( 'general', 'rad_google_maps_api_key' );
		 register_setting( 'general', 'rad_twitter_url' );
		 register_setting( 'general', 'rad_facebook_app_id' );
		 register_setting( 'general', 'rad_facebook_url' );
		 register_setting( 'general', 'rad_instagram_url' );
		 register_setting( 'general', 'rad_contact_email' );
		 register_setting( 'general', 'rad_contact_phone' );
		 register_setting( 'general', 'rad_address' );

		}

		add_action( 'admin_init', 'rad_settings_api_init' );

		function rad_google_analytics_ua_callback() {
		 echo '<input name="rad_google_analytics_ua" id="rad_google_analytics_ua" value="'. get_option( 'rad_google_analytics_ua' ) .'" type="text" class="regular-text" />';
		 echo '<p class="description" id="google-analytics-ua-description">UA-XXXXXX-XX</p>';
		}

		function rad_google_maps_api_key_callback() {
		 echo '<input name="rad_google_maps_api_key" id="rad_google_maps_api_key" value="'. get_option( 'rad_google_maps_api_key' ) .'" type="text" class="regular-text" />';
		}

		function rad_twitter_url_callback() {
		 echo '<input name="rad_twitter_url" id="rad_twitter_url" value="'. get_option( 'rad_twitter_url' ) .'" type="text" class="regular-text" />';
		}

		function rad_facebook_app_id_callback() {
		 echo '<input name="rad_facebook_app_id" id="rad_facebook_app_id" value="'. get_option( 'rad_facebook_app_id' ) .'" type="text" class="regular-text" />';
		}

		function rad_facebook_url_callback() {
		 echo '<input name="rad_facebook_url" id="rad_facebook_url" value="'. get_option( 'rad_facebook_url' ) .'" type="text" class="regular-text" />';
		}

		function rad_instagram_url_callback() {
		 echo '<input name="rad_instagram_url" id="rad_instagram_url" value="'. get_option( 'rad_instagram_url' ) .'" type="text" class="regular-text" />';
		}

		function rad_contact_email_callback() {
		 echo '<input name="rad_contact_email" id="rad_contact_email" value="'. get_option( 'rad_contact_email' ) .'" type="text" class="regular-text" />';
		}

		function rad_contact_phone_callback() {
		 echo '<input name="rad_contact_phone" id="rad_contact_phone" value="'. get_option( 'rad_contact_phone' ) .'" type="text" class="regular-text" />';
		}

		function rad_address_callback() {
		 echo '<input name="rad_address" id="rad_address" value="'. get_option( 'rad_address' ) .'" type="text" class="regular-text" />';
		}
?>
