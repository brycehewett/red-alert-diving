<div class="post__comments">
  <?php if ( post_password_required() ) : ?>
    <p class="comments__nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.'); ?></p>
    </div>
    <?php return; ?>
  <?php endif; ?>

  <?php if ( comments_open() ) : ?>
  <?php endif; ?>

  <?php if ( ! comments_open() ) : ?>
    <p class="post__comments__closed"><?php _e( 'Comments are closed.'); ?></p>
  <?php endif; ?>
</div>
