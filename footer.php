      <?php
        $rad_twitter_url = get_option( 'rad_twitter_url' );
        $rad_facebook_url = get_option( 'rad_facebook_url' );
        $rad_instgram_url = get_option( 'rad_instagram_url' );
        $rad_contact_email = get_option( 'rad_contact_email' );
        $rad_contact_phone = get_option( 'rad_contact_phone' );
        $rad_address = get_option( 'rad_address' );
        $rad_google_analytics_ua = get_option( 'rad_google_analytics_ua' );

        $footer_menu_settings = array(
          'menu'            => 'footer-menu',
          'theme_location'  => 'footer-menu',
          'container'       => '',
          'echo'            => true,
          'fallback_cb'     => false,
          'items_wrap'      => '<nav class="footer__nav-menu-column"><ul>%3$s</ul></nav>'
        );
      ?>

      <footer class="footer">
        <div class="footer__red-bar">
          <?php include('includes/mailchimp-signup-form.php') ?>
        </div>
        <div class="container">
          <div class="footer__logo-row">
            <a class="footer-logo" href="<?php echo get_option('home'); ?>/">
              <img src="<?php bloginfo('template_directory'); ?>/images/RAD-logo-white.svg"/>
            </a>
          </div>
          <?php wp_nav_menu( $footer_menu_settings );
            if ($rad_contact_email || $rad_contact_phone || $rad_address || $rad_twitter_url || $rad_facebook_url || $rad_instgram_url) {
              echo '<div class="footer__address-column">
                      <ul>' .
                        ( $rad_contact_phone ? '<li class="footer__phone"><a href="tel:'. $rad_contact_phone .'">'. $rad_contact_phone .'</a></li>' : '' ) .
                        ( $rad_contact_email ? '<li class="footer__email"><a href="mailto:' . $rad_contact_email . '">' . $rad_contact_email . '</a></li>' : '' ) .
                        ( $rad_address ? '<li class="footer__address">' . $rad_address . '</li>' : '' ) .
                      '</ul>' .
                      ( $rad_twitter_url || $rad_facebook_url || $rad_instgram_url ? '<div class="footer__social-links">' : '' ) .
                        ( $rad_twitter_url ? '<a class="footer__social-links--twitter icon ion-social-twitter" href="' . $rad_twitter_url . '" target="_blank"></a>' : '' ) .
                        ( $rad_facebook_url ? '<a class="footer__social-links--facebook icon ion-social-facebook" href="' . $rad_facebook_url . '" target="_blank"></a>' : '' ) .
                        ( $rad_instgram_url ? '<a class="footer__social-links--instagram icon ion-social-instagram" href="' . $rad_instgram_url . '" target="_blank"></a>' : '' ) .
                      ( $rad_twitter_url || $rad_facebook_url || $rad_instgram_url ? '</div>' : '' ) .
                      '<a href="https://pcbidc.com" target="_blank"><img src="' . get_bloginfo('template_directory') . '/images/PADI-5star-IDC.jpg" /></a>' .
                    '</div>';
            } ?>
        </div>
        <div class="container">
          <small class="footer__copyright">&copy; <?php echo date("Y"); echo " "; bloginfo('name'); ?></small>
        </div>
      </footer>

      <?php wp_footer(); ?>

      <?php
        if ( $rad_google_analytics_ua ) {
            echo "<!-- Global site tag (gtag.js) - Google Analytics -->
                    <script async src='https://www.googletagmanager.com/gtag/js?id=". $rad_google_analytics_ua ."'></script>
                    <script>
                      window.dataLayer = window.dataLayer || [];
                      function gtag(){dataLayer.push(arguments);}
                      gtag('js', new Date());

                      gtag('config', '". $rad_google_analytics_ua ."');
                    </script>";

        }?>

    </div>
  </body>
</html>
